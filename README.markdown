# [iOS Privacy](https://iosprivacy.com/)
`https://iosprivacy.com`
<br><br>
<a href="https://twitter.com/@iOS_Privacy"><img src="https://img.shields.io/twitter/follow/iOS_Privacy?style=social"></a>
<br><br>
<a href="https://hstspreload.org/?domain=iosprivacy.com"><img src="https://img.shields.io/hsts/preload/iosprivacy.com"></a>
<br><br>
<a href="https://observatory.mozilla.org/analyze/iosprivacy.com"><img src="https://img.shields.io/mozilla-observatory/grade-score/iosprivacy.com?publish&style=plastic"></a>
<br><br>
<a href="https://securityheaders.com/?q=iosprivacy.com&followRedirects=on"><img src="https://img.shields.io/security-headers?color=darkblue&url=https%3A%2F%2Fiosprivacy.com"></a>
<br><br>
<a href="https://iosprivacy.com"><img src="https://img.shields.io/website?color=darkred&down_color=lightgrey&down_message=offline&label=website%20is&logo=debian&logoColor=darkred&up_color=blue&up_message=online&url=https%3A%2F%2Fiosprivacy.com"></a>
<hr>
<h3><p xmlns:cc="https://creativecommons.org/ns#" xmlns:dct="https://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://iosprivacy.com">iOS Privacy</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://intr0.com">intr0</a> is licensed under <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-SA 4.0 <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"> <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"> <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"> <img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p></h3>

iVOID.hosts | DomainVoider | Urlhaus
--------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------
![GitLab stars](https://img.shields.io/gitlab/stars/intr0/iVOID.GitLab.io?gitlab_url=https%3A%2F%2Fgitlab.com&style=social) | ![GitLab stars](https://img.shields.io/gitlab/stars/intr0/DomainVoider?gitlab_url=https%3A%2F%2Fgitlab.com&style=social) | ![GitLab stars](https://img.shields.io/gitlab/stars/iosprivacy/CDN?gitlab_url=https%3A%2F%2Fgitlab.com&style=social)

**iOS Privacy serves as a centralized repository for the following filter (block) lists:**

- 1 DomainVoider;
- 2 iVOID.hosts;
- 3 Urlhaus mirror;

## **[DomainVoider](https://iosprivacy.com/domainvoider)**

## **[iVOID.hosts](https://iosprivacy.com/ivoid)**

## **[Urlhaus Mirror](https://iosprivacy.com/urlhaus)**
